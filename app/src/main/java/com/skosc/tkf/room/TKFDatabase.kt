package com.skosc.tkf.room

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    version = 3,
    entities = [Node::class, NodeRelation::class]
)
abstract class TKFDatabase : RoomDatabase() {
    abstract val nodeDao: NodeDao
}