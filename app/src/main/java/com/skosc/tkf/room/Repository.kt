package com.skosc.tkf.room

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class Repository(private val dao: NodeDao) {
    fun addNode(node: Node) {
        Single.fromCallable { dao.insert(node) }
            .subscribeOn(Schedulers.io())
            .subscribe()
    }

    fun removeNode(node: Node) {
        Single.fromCallable {
            dao.delete(node.id)
            dao.deleteRelations(node.id)
        }.subscribeOn(Schedulers.io())
            .subscribe()
    }

    fun removeRelation(parent: Node, child: Node) {
        Single.fromCallable {
            dao.deleteRealtion(parent.id, child.id)
        }.subscribeOn(Schedulers.io())
            .subscribe()
    }

    fun children(node: Node): Observable<List<Node>> {
        return dao.resolvedChildren(node.id)
    }

    fun parents(node: Node): Observable<List<Node>> {
        return dao.resolvedParents(node.id)
    }

    fun notChildren(node: Node): Observable<List<Node>> {
        return dao.resolvedNotChildren(node.id)
    }

    fun find(id: Long): Observable<Node> = dao.byId(id)

    val allNodes: Observable<List<Node>> get() {
        return dao.all()
    }

    fun addParents(to: Node, nodes: List<Node>) {
        Single.fromCallable {
            val rels = nodes.map { NodeRelation(parent = it.id, child = to.id) }
            dao.insert(rels)
        }.subscribeOn(Schedulers.io())
            .subscribe()
    }

    fun addChildren(to: Node, nodes: List<Node>) {
        Single.fromCallable {
            val rels = nodes.map { NodeRelation(child = it.id, parent = to.id) }
            dao.insert(rels)
        }.subscribeOn(Schedulers.io())
            .subscribe()
    }
}