package com.skosc.tkf.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

/**
 * Data class representing node in the graph, can be used both in business and db layers
 */
@Entity(tableName = "node")
data class Node(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Long = 0,

    @ColumnInfo(name = "value")
    val value: String = ""
) {
    @Ignore
    var children: MutableList<Node>? = null

    @Ignore
    var parents: MutableList<Node>? = null

    val isResolved: Boolean get() = children == null || parents == null

    val hasChildren: Boolean get() = children!!.isNotEmpty()

    val hasParents: Boolean get() = parents!!.isNotEmpty()

    override fun equals(other: Any?): Boolean {
        if (other is Node) {
            return id == other.id
        }
        return super.equals(other)
    }
}