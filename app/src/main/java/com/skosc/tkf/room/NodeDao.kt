package com.skosc.tkf.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import io.reactivex.Observable

@Dao
abstract class NodeDao {
    @Insert
    abstract fun insert(node: Node)

    @Insert
    abstract fun insert(rel: List<NodeRelation>)

    @Query("SELECT * FROM node")
    abstract fun allRx(): Observable<List<Node>>

    @Query("DELETE FROM node WHERE node.id == :id")
    abstract fun delete(id: Long)

    @Query("DELETE FROM node_relation WHERE node_relation.child == :id OR node_relation.parent == :id")
    abstract fun deleteRelations(id: Long)

    @Query("DELETE FROM node_relation WHERE node_relation.child == :child AND node_relation.parent == :parent")
    abstract fun deleteRealtion(parent: Long, child: Long)

    @Query(
        """
        SELECT * FROM node
        LEFT JOIN node_relation ON node_relation.parent == :id
        WHERE node.id == node_relation.child
    """
    )
    abstract fun children(id: Long): List<Node>

    @Query(
        """
        SELECT * FROM node
        LEFT JOIN node_relation ON node_relation.child == :id
        WHERE node.id == node_relation.parent
    """
    )
    abstract fun parents(id: Long): List<Node>

    @Query("SELECT * FROM node WHERE node.id == :id")
    abstract fun byId(id: Long): Observable<Node>


    val resolved: (Node) -> Node = {
        val parents = parents(it.id)
        val children = children(it.id)
        it.children = children.toMutableList()
        it.parents = parents.toMutableList()
        it
    }

    fun all(): Observable<List<Node>> {
        return allRx().map {
            return@map it.map(resolved)
        }
    }

    @Query(
        """
        SELECT * FROM node
        LEFT JOIN node_relation ON node_relation.parent == :id
        WHERE node.id == node_relation.child
    """
    )
    abstract fun childrenRx(id: Long): Observable<List<Node>>

    @Query(
        """
        SELECT * FROM node
        LEFT JOIN node_relation ON node_relation.child == :id
        WHERE node.id == node_relation.parent
    """
    )
    abstract fun parentsRx(id: Long): Observable<List<Node>>

    @Query(
        """
        SELECT * FROM node
        WHERE node.id != :id
    """
    )
    abstract fun notChildrenRx(id: Long): Observable<List<Node>>

    fun resolvedChildren(id: Long): Observable<List<Node>> {
        return childrenRx(id).map {
            return@map it.map(resolved)
        }
    }

    fun resolvedParents(id: Long): Observable<List<Node>> {
        return parentsRx(id).map {
            return@map it.map(resolved)
        }
    }

    fun resolvedNotChildren(id: Long): Observable<List<Node>> {
        return notChildrenRx(id).map {
            return@map it.map(resolved)
        }
    }
}
