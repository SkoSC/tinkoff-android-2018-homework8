package com.skosc.tkf.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.skosc.tkf.fragment.NodeChildListFragment
import com.skosc.tkf.fragment.NodeParentListFragment

class NodeListPagerAdapter(private val id: Long, fm: FragmentManager) : FragmentPagerAdapter(fm) {
    private val tabs = listOf(
        NodeChildListFragment.new(id),
        NodeParentListFragment.new(id)
    )

    override fun getItem(position: Int): Fragment {
        return tabs[position]
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "Children"
            1 -> "Parents"
            else -> throw IllegalStateException("No fragment at position: $position")
        }
    }

}