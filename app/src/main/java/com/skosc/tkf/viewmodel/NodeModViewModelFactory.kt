package com.skosc.tkf.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import org.kodein.di.Kodein
import org.kodein.di.direct
import org.kodein.di.generic.instance

class NodeModViewModelFactory(private val kodein: Kodein, private val rootId: Long) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return NodeModViewModel(rootId, kodein.direct.instance()) as T
    }
}