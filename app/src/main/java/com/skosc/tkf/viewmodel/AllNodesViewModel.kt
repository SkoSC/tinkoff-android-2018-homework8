package com.skosc.tkf.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.skosc.tkf.room.Node
import com.skosc.tkf.room.Repository
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import java.util.concurrent.TimeUnit

class AllNodesViewModel(private val repo: Repository) : ViewModel() {
    private val cdisp = CompositeDisposable()

    val nodes = MutableLiveData<List<Node>>()

    init {
        val disp = Observable.interval(1, TimeUnit.SECONDS).flatMap { repo.allNodes  }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                nodes.value = it
            }
        cdisp.add(disp)
    }

    fun addNode(text: String) {
        repo.addNode(
            Node(
                value = text
            )
        )
    }

    fun removeNode(node: Node) {
        repo.removeNode(node)
    }

    override fun onCleared() {
        super.onCleared()
        cdisp.dispose()
    }
}