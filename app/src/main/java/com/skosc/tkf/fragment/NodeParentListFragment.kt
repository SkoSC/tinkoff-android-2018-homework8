package com.skosc.tkf.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.MaterialDialog
import com.skosc.tkf.R
import com.skosc.tkf.TKFApplication
import com.skosc.tkf.adapter.NodeRecyclerAdapter
import com.skosc.tkf.room.Node
import com.skosc.tkf.viewmodel.NodeModViewModel
import com.skosc.tkf.viewmodel.NodeModViewModelFactory
import kotlinx.android.synthetic.main.fragment_node_list.*
import org.kodein.di.direct
import org.kodein.di.generic.instance


class NodeParentListFragment : Fragment() {
    companion object {
        fun new(id: Long): Fragment {
            return NodeParentListFragment().apply {
                arguments = bundleOf("id" to id)
            }
        }
    }

    val rootId: Long by lazy { arguments!!.getLong("id") }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_node_list, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onStart() {
        super.onStart()

        val app = activity!!.application as TKFApplication
        val factory: NodeModViewModelFactory = app.kodein.direct.instance(arg = rootId)
        val vm = ViewModelProviders.of(this, factory).get(NodeModViewModel::class.java)


        recycler.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        val onClick: (Node) -> Unit = { node ->
            MaterialDialog(context!!)
                .title(text = "Parents")
                .positiveButton(text = "Add Parent") { vm.addParents(listOf(node)) }
                .negativeButton(text = "Remove Relation") { vm.deleteRelationWith(node) }
                .show()
        }
        val adapter = NodeRecyclerAdapter(onClick)
        recycler.adapter = adapter

        vm.currentNode.observe(this, Observer {
            header.text = "Node: " + it.value
        })

        vm.parents.observe(this, Observer {
            adapter.submitItems(it.distinctBy { it.id })
        })
    }

}
